FROM metabase/metabase:latest

RUN \
    set -o xtrace && \
    # Add non-root user metabase && \
    addgroup -S metabase && \
    adduser -D -G metabase metabase && \
    mkdir -p /metabase.db && \
    chown metabase:metabase /metabase.db

# expose our default runtime port
EXPOSE 3000

# run it
# ENTRYPOINT ["/app/bin/start"]
